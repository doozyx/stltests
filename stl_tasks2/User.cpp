#include <utility>

#include "User.h"

int User::idGenerator = 0;

User::User(std::string name) : mName(std::move(name)) {
    mId = ++idGenerator;
}

User::User(std::string name, int age, int height, std::set<std::string> hobbies, User::Gender gender,
           std::set<int> friends) : mName(std::move(name)), mAge(age), mHeight(height),
                                            mHobbies(std::move(hobbies)),
                                            mGender(gender), mFriends(std::move(friends)) {
    mId = ++idGenerator;
}

int User::getId() const {
    return mId;
}

const std::string &User::getName() const {
    return mName;
}

int User::getAge() const {
    return mAge;
}

int User::getHeight() const {
    return mHeight;
}

const std::set<std::string> &User::getHobbies() const {
    return mHobbies;
}

User::Gender User::getGender() const {
    return mGender;
}

const std::set<int> &User::getFriends() const {
    return mFriends;
}
