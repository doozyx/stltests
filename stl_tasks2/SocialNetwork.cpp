#include "SocialNetwork.h"

#include <algorithm>

int SocialNetwork::addUser(std::string name, int age, int height, std::set<std::string> hobbies, User::Gender gender,
                           std::set<int> friends) {
    auto newUser = std::make_shared<User>(name, age, height, hobbies, gender, friends);
    mUsers.insert({newUser->getId(), newUser});
    mNameIndex.insert({newUser->getName(), newUser});
    if (age != 0) {
        mAgeIndex.insert({newUser->getAge(), newUser});
    }
    if (!hobbies.empty()) {
        for (const auto &hobby: hobbies) {
            mHobbiesIndex.insert({hobby, newUser});
        }
    }
    return newUser->getId();
}

bool SocialNetwork::deleteUser(int id) {
    auto userIterator = mUsers.find(id);
    if (userIterator == mUsers.end()) {
        return false;
    }
    auto user = userIterator->second;
    auto namesIterator = mNameIndex.equal_range(user->getName());
    for (auto it = namesIterator.first; it != namesIterator.second; ++it) {
        if (it->second.get() == user.get()) {
            mNameIndex.erase(it);
            break;
        }
    }
    auto ageIterator = mAgeIndex.equal_range(user->getAge());
    for (auto it = ageIterator.first; it != ageIterator.second; ++it) {
        if (it->second.get() == user.get()) {
            mAgeIndex.erase(it);
            break;
        }
    }
    for (const auto &hobby: user->getHobbies()) {
        auto tagsIterator = mHobbiesIndex.equal_range(hobby);
        for (auto it = tagsIterator.first; it != tagsIterator.second; ++it) {
            if (it->second.get() == user.get()) {
                mHobbiesIndex.erase(it);
                break;
            }
        }
    }
    return mUsers.erase(id);
}

SocialNetwork::UserPtrList SocialNetwork::searchUserByName(const std::string &name) {
    SocialNetwork::UserPtrList users;

    auto notesIterator = mNameIndex.equal_range(name);
    std::transform(notesIterator.first, notesIterator.second, back_inserter(users),
                   [](std::pair<std::string, std::shared_ptr<User>> element) { return element.second; });

    return users;
}

SocialNetwork::UserPtrList SocialNetwork::searchUserByAge(int age) {
    SocialNetwork::UserPtrList users;

    auto notesIterator = mAgeIndex.equal_range(age);
    std::transform(notesIterator.first, notesIterator.second, back_inserter(users),
                   [](std::pair<int, std::shared_ptr<User>> element) { return element.second; });

    return users;
}

SocialNetwork::UserPtrList SocialNetwork::searchUserByHobbies(std::initializer_list<std::string> hobbies) {
    std::set<std::shared_ptr<User>> usersSet;

    for (const auto &hobby: hobbies) {
        auto notesIterator = mHobbiesIndex.equal_range(hobby);
        std::transform(notesIterator.first, notesIterator.second, std::inserter(usersSet, usersSet.begin()),
                       [](std::pair<std::string, std::shared_ptr<User>> element) { return element.second; });
    }

    SocialNetwork::UserPtrList users;
    users.insert(users.end(), usersSet.begin(), usersSet.end());

    return users;
}

std::set<int> SocialNetwork::getFriendsOfUser(int id) {
    auto userIterator = mUsers.find(id);
    if (userIterator == mUsers.end()) {
        return {};
    }
    auto user = userIterator->second.get();
    if (user == nullptr) {
        return {};
    }
    return user->getFriends();
}

int SocialNetwork::userCount() {
    return mUsers.size();
}




