#include <iostream>

#include <cassert>

#include "SocialNetwork.h"

void testInsert() {
    SocialNetwork socialNetwork;

    auto id = socialNetwork.addUser("doozy", 22, 170, {"running", "skiing"}, User::MALE);
    assert(id != 0);
    assert(socialNetwork.userCount() == 1);
}

void testDelete() {
    SocialNetwork socialNetwork;

    auto id = socialNetwork.addUser("doozy", 22, 170, {"running", "skiing"}, User::MALE);
    assert(id != 0);
    assert(socialNetwork.userCount() == 1);

    auto deleteOK = socialNetwork.deleteUser(id);
    assert(deleteOK);
    assert(socialNetwork.userCount() == 0);
}

void testSearchByName() {
    SocialNetwork socialNetwork;

    socialNetwork.addUser("doozy", 22, 170, {"running", "skiing"}, User::MALE);
    socialNetwork.addUser("doozy2", 22, 170, {"running", "skiing", "biking"}, User::MALE);
    socialNetwork.addUser("doozy", 30);


    auto results1 = socialNetwork.searchUserByName("doozy");
    assert(results1.size() == 2);
    assert(results1.front()->getName() == "doozy");
    assert(results1.front()->getAge() == 22);
    assert(results1.at(1)->getName() == "doozy");
    assert(results1.at(1)->getAge() == 30);

    auto results2 = socialNetwork.searchUserByName("doozy2");
    assert(results2.size() == 1);
    assert(results2.front()->getName() == "doozy2");

    auto results3 = socialNetwork.searchUserByName("dzx");
    assert(results3.empty());
}

void testSearchByAge() {
    SocialNetwork socialNetwork;

    socialNetwork.addUser("doozy", 22, 170, {"running", "skiing"}, User::MALE);
    socialNetwork.addUser("doozy2", 30, 170, {"running", "skiing", "biking"}, User::MALE);
    socialNetwork.addUser("doozy", 22);


    auto results1 = socialNetwork.searchUserByAge(22);
    assert(results1.size() == 2);
    assert(results1.front()->getName() == "doozy");
    assert(results1.front()->getAge() == 22);
    assert(results1.at(1)->getName() == "doozy");
    assert(results1.at(1)->getAge() == 22);

    auto results2 = socialNetwork.searchUserByAge(30);
    assert(results2.size() == 1);
    assert(results2.front()->getName() == "doozy2");

    auto results3 = socialNetwork.searchUserByAge(50);
    assert(results3.empty());
}

void testSearchByHobbies() {
    SocialNetwork socialNetwork;

    socialNetwork.addUser("doozy", 22, 170, {"running", "skiing"}, User::MALE);
    socialNetwork.addUser("doozy2", 22, 170, {"running", "skiing", "biking"}, User::MALE);
    socialNetwork.addUser("doozy3", 30, 170, {"gaming"});


    auto results1 = socialNetwork.searchUserByHobbies({"running", "gaming"});
    assert(results1.size() == 3);
    assert(results1.front()->getName() == "doozy");
    assert(results1.at(1)->getName() == "doozy2");
    assert(results1.at(2)->getName() == "doozy3");

    auto results2 = socialNetwork.searchUserByHobbies({"biking"});
    assert(results2.size() == 1);
    assert(results2.front()->getName() == "doozy2");

    auto results3 = socialNetwork.searchUserByHobbies({"sleeping"});
    assert(results3.empty());
}

void testGetFriendsOfUser() {
    SocialNetwork socialNetwork;

    auto user1 = socialNetwork.addUser("doozy", 22, 170, {"running", "skiing"}, User::MALE, {1, 2, 3});
    auto user2 = socialNetwork.addUser("doozy2", 22, 170, {"running", "skiing", "biking"}, User::MALE);
    auto user3 = socialNetwork.addUser("doozy", 30, 170, {"running", "skiing", "biking"}, User::MALE, {1, 2});

    auto results1 = socialNetwork.getFriendsOfUser(user1);
    assert(results1.size() == 3);
    assert(results1.find(1) != results1.end());
    assert(results1.find(2) != results1.end());
    assert(results1.find(3) != results1.end());

    auto results2 = socialNetwork.getFriendsOfUser(user2);
    assert(results2.empty());

    auto results3 = socialNetwork.getFriendsOfUser(user3);
    assert(results3.size() == 2);
    assert(results3.find(1) != results1.end());
    assert(results3.find(2) != results1.end());
}


int main() {
    testInsert();
    testDelete();
    testSearchByName();
    testSearchByAge();
    testSearchByHobbies();
    testGetFriendsOfUser();
    return 0;
}