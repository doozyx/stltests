#pragma once

#include <string>
#include <set>

class User {
public:
    enum Gender {
        MALE,
        FEMALE
    };

    explicit User(std::string name);
    explicit User(std::string name, int age, int height, std::set<std::string> hobbies, Gender gender,
         std::set<int> friends);

    int getId() const;

    const std::string &getName() const;

    int getAge() const;

    int getHeight() const;

    const std::set<std::string> &getHobbies() const;

    Gender getGender() const;

    const std::set<int> &getFriends() const;

private:
    static int idGenerator;

    int mId{};
    std::string mName;
    int mAge{};
    int mHeight{};
    std::set<std::string> mHobbies;
    Gender mGender{};
    std::set<int> mFriends;
};
