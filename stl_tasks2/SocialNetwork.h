#pragma once

#include <string>
#include <set>
#include <vector>
#include <map>
#include <memory>

#include "User.h"

class SocialNetwork {
    typedef std::multimap<std::string, std::shared_ptr<User>> NotePtrStringIndexMap;
    typedef std::multimap<int, std::shared_ptr<User>> NotePtrIntIndexMap;
    typedef std::vector<std::shared_ptr<User>> UserPtrList;
public:
    int addUser(std::string name, int age = {}, int height = {}, std::set<std::string> hobbies = {},
                 User::Gender gender = {}, std::set<int> friends = {});

    bool deleteUser(int id);

    UserPtrList searchUserByName(const std::string& name);

    UserPtrList searchUserByAge(int age);

    UserPtrList searchUserByHobbies(std::initializer_list<std::string> hobbies);

    std::set<int> getFriendsOfUser(int id);

    int userCount();

private:
    std::map<int, std::shared_ptr<User>> mUsers;
    NotePtrStringIndexMap mNameIndex;
    NotePtrIntIndexMap mAgeIndex;
    NotePtrStringIndexMap mHobbiesIndex;
};

