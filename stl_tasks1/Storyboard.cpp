#include "Storyboard.h"

#include <algorithm>
#include <set>

int Storyboard::addNote(const std::string &title, const std::string &text, const std::vector<std::string> &tags) {
    auto newNote = std::make_shared<Note>(title, text, tags);
    mNotes.insert({newNote->getId(), newNote});
    mTitlesIndex.insert({newNote->getTitle(), newNote});
    mTextIndex.insert({newNote->getText(), newNote});
    for (const auto &tag: tags) {
        mTagIndex.insert({tag, newNote});
    }
    return newNote->getId();
}

bool Storyboard::deleteNote(int id) {
    auto noteIterator = mNotes.find(id);
    if (noteIterator == mNotes.end()) {
        return false;
    }
    auto note = noteIterator->second;
    auto titlesIterator = mTitlesIndex.equal_range(note->getTitle());
    for (auto it = titlesIterator.first; it != titlesIterator.second; ++it) {
        if (it->second.get() == note.get()) {
            mTitlesIndex.erase(it);
            break;
        }
    }
    auto textIterator = mTextIndex.equal_range(note->getText());
    for (auto it = textIterator.first; it != textIterator.second; ++it) {
        if (it->second.get() == note.get()) {
            mTextIndex.erase(it);
            break;
        }
    }
    for (const auto& tag: note->getTags()) {
        auto tagsIterator = mTagIndex.equal_range(tag);
        for (auto it = tagsIterator.first; it != tagsIterator.second; ++it) {
            if (it->second.get() == note.get()) {
                mTagIndex.erase(it);
                break;
            }
        }
    }
    return mNotes.erase(id);
}

std::vector<std::shared_ptr<Note>> Storyboard::searchByTitle(const std::string &title) {
    std::vector<std::shared_ptr<Note>> notes;

    auto notesIterator = mTitlesIndex.equal_range(title);
    std::transform(notesIterator.first, notesIterator.second, back_inserter(notes),
                   [](std::pair<std::string, std::shared_ptr<Note>> element) { return element.second; });

    return notes;
}

std::vector<std::shared_ptr<Note>> Storyboard::searchByText(const std::string &text) {
    std::vector<std::shared_ptr<Note>> notes;

    auto notesIterator = mTextIndex.equal_range(text);
    std::transform(notesIterator.first, notesIterator.second, back_inserter(notes),
                   [](std::pair<std::string, std::shared_ptr<Note>> element) { return element.second; });

    return notes;
}

std::vector<std::shared_ptr<Note>> Storyboard::searchByTag(const std::string &tag) {
    std::vector<std::shared_ptr<Note>> notes;

    auto notesIterator = mTagIndex.equal_range(tag);
    std::transform(notesIterator.first, notesIterator.second, back_inserter(notes),
                   [](std::pair<std::string, std::shared_ptr<Note>> element) { return element.second; });

    return notes;
}

int Storyboard::notesCount() {
    return mNotes.size();
}

Storyboard::NotePtrList Storyboard::searchByTag(std::initializer_list<std::string> tags) {
    std::set<std::shared_ptr<Note>> notesSet;

    for (const auto &tag: tags) {
        auto notesIterator = mTagIndex.equal_range(tag);
        std::transform(notesIterator.first, notesIterator.second, std::inserter(notesSet, notesSet.begin()),
                       [](std::pair<std::string, std::shared_ptr<Note>> element) { return element.second; });
    }

    Storyboard::NotePtrList  notes;
    notes.insert(notes.end(), notesSet.begin(), notesSet.end());

    return notes;
}
