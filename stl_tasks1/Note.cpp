#include <utility>

#include "Note.h"

int Note::idGenerator = 0;

Note::Note(std::string title, std::string text, std::vector<std::string> tags) : mTitle(std::move(title)),
                                                                                 mText(std::move(text)),
                                                                                 mTags(std::move(tags)) {
    mId = ++idGenerator;
}

const std::string &Note::getTitle() const {
    return mTitle;
}

const std::string &Note::getText() const {
    return mText;
}


const std::vector<std::string> &Note::getTags() const {
    return mTags;
}

int Note::getId() const {
    return mId;
}

