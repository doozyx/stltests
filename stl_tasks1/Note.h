#pragma once

#include <string>
#include <vector>

class Note {
public:
    explicit Note(std::string title, std::string text, std::vector<std::string> tags);

    int getId() const;

    const std::string &getTitle() const;

    const std::string &getText() const;

    const std::vector<std::string> &getTags() const;

private:
    static int idGenerator;

    int mId;

    std::string mTitle;
    std::string mText;
    std::vector<std::string> mTags;

};
