#include <cassert>

#include "Storyboard.h"

void testInsert() {
    Storyboard storyboard;

    auto id = storyboard.addNote("Test Traceplayer",
                                 "Implement a unit test for the class Traceplayer of the spark core framework.",
                                 {"unit test", "traceplayer", "testing", "spark core"});
    assert(id != 0);
    assert(storyboard.notesCount() == 1);
}

void testDelete() {
    Storyboard storyboard;

    auto id = storyboard.addNote("Test Traceplayer",
                                 "Implement a unit test for the class Traceplayer of the spark core framework.",
                                 {"unit test", "traceplayer", "testing", "spark core"});
    assert(id != 0);
    assert(storyboard.notesCount() == 1);

    auto deleteOK = storyboard.deleteNote(id);
    assert(deleteOK);
    assert(storyboard.notesCount() == 0);
}

void testSearchByTitle() {
    Storyboard storyboard;

    storyboard.addNote("Test Traceplayer",
                       "Implement a unit test for the class Traceplayer of the spark core framework.",
                       {"unit test", "traceplayer", "testing", "spark core"});
    storyboard.addNote("Test Traceplayer",
                       "Implement a unit test for the class Traceplayer of the spark core framework.",
                       {"unit test", "traceplayer", "testing", "spark core"});
    storyboard.addNote("Test Traceplayer",
                       "Implement a unit test for the class Traceplayer of the spark core framework.",
                       {"unit test", "traceplayer", "testing", "spark core"});
    storyboard.addNote("Test Traceplayer1",
                       "Implement a unit test for the class Traceplayer of the spark core framework.",
                       {"unit test", "traceplayer", "testing", "spark core"});

    auto results1 = storyboard.searchByTitle("Test Traceplayer");
    assert(results1.size() == 3);
    assert(results1.front()->getTitle() == "Test Traceplayer");
    assert(results1.at(1)->getTitle() == "Test Traceplayer");
    assert(results1.at(2)->getTitle() == "Test Traceplayer");

    auto results2 = storyboard.searchByTitle("Test Traceplayer1");
    assert(results2.size() == 1);
    assert(results2.front()->getTitle() == "Test Traceplayer1");

    auto results3 = storyboard.searchByTitle("Test Traceplayer2");
    assert(results3.empty());
}

void testSearchByText() {
    Storyboard storyboard;

    storyboard.addNote("Test Traceplayer",
                       "Implement a unit test for the class Traceplayer of the spark core framework.",
                       {"unit test", "traceplayer", "testing", "spark core"});
    storyboard.addNote("Test Traceplayer",
                       "Implement a unit test for the class Traceplayer of the spark core framework.",
                       {"unit test", "traceplayer", "testing", "spark core"});
    storyboard.addNote("Test Traceplayer1",
                       "Implement a unit test for the class Traceplayer of the spark core framework.",
                       {"unit test", "traceplayer", "testing", "spark core"});
    storyboard.addNote("Test Traceplayer1",
                       "Implement a unit test for the class Traceplayer of the spark core framework.1",
                       {"unit test", "traceplayer", "testing", "spark core"});

    auto results1 = storyboard.searchByText("Implement a unit test for the class Traceplayer of the spark core framework.");
    assert(results1.size() == 3);
    assert(results1.front()->getText() == "Implement a unit test for the class Traceplayer of the spark core framework.");
    assert(results1.front()->getTitle() == "Test Traceplayer");
    assert(results1.at(1)->getText() == "Implement a unit test for the class Traceplayer of the spark core framework.");
    assert(results1.at(2)->getText() == "Implement a unit test for the class Traceplayer of the spark core framework.");
    assert(results1.at(2)->getTitle() == "Test Traceplayer1");

    auto results2 = storyboard.searchByText("Implement a unit test for the class Traceplayer of the spark core framework.1");
    assert(results2.size() == 1);
    assert(results2.front()->getText() == "Implement a unit test for the class Traceplayer of the spark core framework.1");

    auto results3 = storyboard.searchByText("Test Traceplayer2");
    assert(results3.empty());
}

void testSearchByTag() {
    Storyboard storyboard;

    storyboard.addNote("Test Traceplayer",
                       "Implement a unit test for the class Traceplayer of the spark core framework.",
                       {"unit test", "traceplayer1", "testing", "spark core"});
    storyboard.addNote("Test Traceplayer",
                       "Implement a unit test for the class Traceplayer of the spark core framework.",
                       {"unit test", "traceplayer1", "testing1", "spark core"});
    storyboard.addNote("Test Traceplayer1",
                       "Implement a unit test for the class Traceplayer of the spark core framework.",
                       {"unit test", "traceplayer", "testing2", "spark core"});
    storyboard.addNote("Test Traceplayer1",
                       "Implement a unit test for the class Traceplayer of the spark core framework.1",
                       {"unit test", "traceplayer", "testing3", "spark core"});

    auto results1 = storyboard.searchByTag("unit test");
    assert(results1.size() == 4);

    auto results2 = storyboard.searchByTag("traceplayer1");
    assert(results2.size() == 2);

    auto results3 = storyboard.searchByTag("testing3");
    assert(results3.size() == 1);

    auto results4 = storyboard.searchByTag({"testing3", "testing2"});
    assert(results4.size() == 2);
}

int main() {
    testInsert();
    testDelete();
    testSearchByTitle();
    testSearchByText();
    testSearchByTag();

    return 0;
}