#pragma once

#include <string>
#include <vector>
#include <map>
#include <memory>

#include "Note.h"

class Storyboard {
    typedef std::multimap<std::string, std::shared_ptr<Note>> NotePtrIndexMap;
    typedef std::vector<std::shared_ptr<Note>> NotePtrList;
public:
    int addNote(const std::string& title, const std::string& text, const std::vector<std::string>& tags);
    bool deleteNote(int id);

    NotePtrList searchByTitle(const std::string& title);
    NotePtrList searchByText(const std::string& text);
    NotePtrList searchByTag(const std::string& tag);
    NotePtrList searchByTag(std::initializer_list<std::string> tags);

    int notesCount();

private:
    std::map<int, std::shared_ptr<Note>> mNotes;
    NotePtrIndexMap mTitlesIndex;
    NotePtrIndexMap mTextIndex;
    NotePtrIndexMap mTagIndex;
};

